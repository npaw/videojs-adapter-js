## [6.8.22] - 2025-03-11
### Library
- Update `youboralib` to `6.8.60`.

## [6.8.21] - 2024-10-10
### Library
- Update `youboralib` to `6.8.59`.

## [6.8.20] - 2024-07-10
### Library
- Update `youboralib` to `6.8.57`.
-
## [6.8.19] - 2024-05-09
### Library
- Update `youboralib` to `6.8.55`.

## [6.8.18] - 2024-04-09
### Library
- Update `youboralib` to `6.8.54`.

## [6.8.17] - 2024-03-06
### Library
- Update `youboralib` to `6.8.52`.

## [6.8.16] - 2024-01-18
### Library
- Update `youboralib` to `6.8.51`.

## [6.8.15] - 2023-12-21
### Library
- Update `youboralib` to `6.8.50`.
- Update `youboralib-util` to `1.4.3`.

## [6.8.14] - 2023-05-23
### Fixed
- Typo in the playerListener function.

## [6.8.13] - 2023-04-21
### Fixed
- Condition play and ended listeners, in videojs adapter, if is into an AdBreak

## [6.8.12] - 2022-10-25
### Fixed
- Report triggered events parameter for the fired plugin events
### Library
- Packaged with `lib 6.8.33`

## [6.8.11] - 2022-09-22
### Fixed
- For shaka tech, support get resource with getCurrentScr function
- For shaka tech, support get bitrate using current video track
### Library
- Packaged with `lib 6.8.24`

## [6.8.10] - 2022-09-22
### Fixed
- Null check for plugin object on loadAdsAdapter
### Library
- Packaged with `lib 6.8.24`

## [6.8.9] - 2022-07-04
### Fixed
 - IMA ads click URL detection for new IMA builds
### Library
 - Packaged with `lib 6.8.24`

## [6.8.8] - 2022-05-16
### Fixed
 - Buffer monitor detection triggered too often
 - New detection method to get information from shaka instances
### Library
 - Packaged with `lib 6.8.19`

## [6.8.7] - 2022-04-11
### Fixed
 - IMA ads click URL detection

## [6.8.6] - 2022-04-08
### Fixed
 - Added freewheel additional case to track ads with more detailed data
### Library
 - Packaged with `lib 6.8.15`

## [6.8.5] - 2022-02-08
### Fixed
 - Null check for `google.ima` parameter
### Library
 - Packaged with `lib 6.8.12`

## [6.8.4] - 2021-12-01
### Fixed
 - Buffers not detected when seeking event triggers before playhead stops to move

## [6.8.3] - 2021-11-26
### Fixed
 - Wrong bitrate collection for contrib hls, reverted change with additional logic changes so media bandwidth is the first option
### Library
 - Packaged with `lib 6.8.9`

## [6.8.2] - 2021-11-15
### Fixed
 - Error when an adapter instance not set in the plugin starts the timer to detect jointime
 - Error when trying to get ad error information when using videojs-contrib-ads
### Library
 - Packaged with `lib 6.8.7`

## [6.8.1] - 2021-10-27
### Fixed
 - Wrong bitrate collection for contrib hls, reverted change
 - Removed removeEventListener from ima adapter, since ima has no way to remove listeners
### Library
 - Packaged with `lib 6.8.5`

## [6.8.0] - 2021-09-15
### Fixed
 - Fake buffer being reported sometimes when playing ima ads
### Library
 - Packaged with `lib 6.8.0`

## [6.7.20] - 2021-08-09
### Fixed
 - Updated the way bitrate is get for contrib hls

## [6.7.19] - 2021-08-06
### Added
 - Contrib dash as a tech
 ### Library
 - Packaged with `lib 6.7.42`

## [6.7.18] - 2021-07-12
### Added
 - Now dispose event deletes the adapter

## [6.7.17] - 2021-06-28
### Added
 - Use of `vhs` api instead of hls when available

## [6.7.16] - 2021-06-18
### Library
 - Packaged with `lib 6.7.40`

## [6.7.15] - 2021-06-02
### Added
 - Third fallback for bitrate for contrib hls.
### Library
 - Packaged with `lib 6.7.35`

## [6.7.14] - 2021-05-04
### Fixed
 - Wrong url getter ror dash shaka tech
### Library
 - Packaged with `lib 6.7.33`

## [6.7.13] - 2020-12-04
### Library
 - Packaged with `lib 6.7.24`

## [6.7.12] - 2020-11-11
### Fixed
 - Removed deprecated access to tech hls, using vhs instead when available.

## [6.7.11] - 2020-10-30
### Fixed
 - Allow to set external values for `content.id`
### Library
 - Packaged with `lib 6.7.22`

## [6.7.10] - 2020-08-05
### Fixed
 - `Object.keys` removed, using a `for` loop instead to support old browsers
### Library
 - Packaged with `lib 6.7.14`

## [6.7.9] - 2020-07-15
### Added
 - Video ID for brightcove

## [6.7.8] - 2020-07-03
### Library
 - Packaged with `lib 6.7.11`

## [6.7.7] - 2020-05-25
### Fixed
 - Included 1002 code as fatal error
### Added
 - Auto subtitles language reporting
 - `errorModifyer` method for youbora classic integration and `fatalErrors`, `nonFatalErrors` and `ignoreErrors` for youbora videojs framework integration to change the behaviour of errors.

## [6.7.6] - 2020-04-30
### Fixed
 - Improved preroll detection for brightcove ima
 - Added case of errors without code

## [6.7.5] - 2020-04-28
### Library
 - Packaged with `lib 6.7.6`

## [6.7.4] - 2020-04-16
### Library
 - Packaged with `lib 6.7.5`

## [6.7.3] - 2020-04-15
### Library
 - Packaged with `lib 6.7.4`

## [6.7.2] - 2020-04-14
### Added
 - TotalBytes getter for contrib-hls
### Library
 - Packaged with `lib 6.7.3`

## [6.7.1] - 2020-03-31
### Fixed
 - Check for ads adapter without events after setting adapter
 - Check for ima objects to avoid incomplete report of the events
### Added
 - Bitrate double detection for hls contribç
 - Improved ad system detection
### Removed
 - Abort listener

## [6.7.0] - 2020-03-04
### Library
 - Packaged with `lib 6.7.0`

## [6.5.14] - 2020-01-13
### Library
 - Packaged with `lib 6.5.24`

## [6.5.13] - 2020-01-03
### Fixed
 - Ima adPosition, adUrl, cuepoints and playhead
### Library
 - Packaged with `lib 6.5.23`

## [6.5.12] - 2019-12-18
### Library
 - Packaged with `lib 6.5.22`

## [6.5.11] - 2019-12-05
### Fixed
 - Resource not being reported in some cases
### Library
 - Packaged with `lib 6.5.20`

## [6.5.10] - 2019-11-12
### Added
 - Extra IMA errors (mostly for manifest failures)

## [6.5.9] - 2019-11-12
### Changed
 - Code refactor
### Added
 - Support for videojs-contrib-quality-levels renditions and bitrate

## [6.5.8] - 2019-11-05
### Library
 - Packaged with `lib 6.5.19`

## [6.5.7] - 2019-10-11
### Added
 - Extra metadata for freewheel brightcove: title, resource, adurl, breakstime, creativeid, manifest errors, and code and message for errors

## [6.5.6] - 2019-10-04
### Fix
 - Playhead for adstop in freewheel brightcove
### Library
 - Packaged with `lib 6.5.16`

## [6.5.5] - 2019-09-09
### Fix
 - Generic ad listeners for ima, to listen DAI ads in some cases.
### Library
 - Packaged with `lib 6.5.14`

## [6.5.4] - 2019-07-30
### Fix
 - Seek not being sent when seeking to the beginning
### Library
 - Packaged with `lib 6.5.9`

## [6.5.3] - 2019-07-25
### Fix
 - Recovering views after fatal error
### Library
 - Packaged with `lib 6.5.8`

## [6.5.2] - 2019-07-09
### Fix
 - SSAI support (version)
### Added
 - Dash shakaPlayer tech support
### Library
 - Packaged with `lib 6.5.6`

## [6.5.1] - 2019-05-31
### Fix
 - Listening ad platforms even if the event 'adsready' is not triggered (DAI)
 ### Library
 - Packaged with `lib 6.5.5`

## [6.5.0] - 2019-05-31
### Added
 - SmartAds 3.0 support

## [6.4.13] - 2019-05-28
### Fix
 - Polling system to detect jointime when player crashes and doesnt report timeupdate and playing events

## [6.4.12] - 2019-05-23
### Fix
 - Brightcove ads not being tracked sometimes
### Library
 - Packaged with `lib 6.4.29`

## [6.4.11] - 2019-05-22
### Fix
 - Jointime when playing is not triggered in live with negative playhead values

## [6.4.10] - 2019-05-02
### Added
 - `isLive` getter for brigthcove
 - Changed register and unregister listeners method
### Library
 - Packaged with `lib 6.4.26`

## [6.4.9] - 2019-04-23
### Added
 - Brightcove features
### Library
 - Packaged with `lib 6.4.25`

## [6.4.8] - 2019-04-10
### Fix
 - Ima3 for brigthcove, without freewheel plugin
### Library
 - Packaged with `lib 6.4.23`

## [6.4.7] - 2019-03-28
### Fix
 - Fake view created at the beggining if init had no resource

## [6.4.6] - 2019-03-18
### Fix
 - Monitor undefined check

## [6.4.5] - 2019-01-30
### Fix
 - Start not sent when autoplay is blocked (additional case)
### Library
 - Packaged with `lib 6.4.15`

## [6.4.4] - 2019-01-10
### Fix
 - Resource when using hls not being reported always
### Library
 - Packaged with `lib 6.4.13`

## [6.4.3] - 2019-01-03
### Fix
 - Start not sent when autoplay is blocked

## [6.4.2] - 2018-12-19
### Library
 - Packaged with `lib 6.4.12`
### Fix
 - Start not being sent always when play button is pressed
 - Blocked autoplay detection

## [6.4.1] - 2018-10-22
### Library
 - Packaged with `lib 6.4.9`

## [6.4.0] - 2018-10-03
### Library
 - Packaged with `lib 6.4.8`

## [6.3.4] - 2018-08-07
### Fix
 - Preroll not tracked with ima on android chrome
 - Only first ad of each group tracked

## [6.3.3] - 2018-08-07
### Fix
 - Crashing when player stops and adapter is not set

## [6.3.2] - 2018-08-03
### Fix
 - Prevent additional views created to report the same error for safari browser
### Library
 - Packaged with `lib 6.3.5`

## [6.3.1] - 2018-07-25
### Fix
 - Prevent additional views created to report the same error
### Library
 - Packaged with `lib 6.3.4`

## [6.3.0] - 2018-05-29
### Library
 - Packaged with `lib 6.3.0`

 ## [6.2.3] - 2018-05-17
### Fix
 - Fix for error not stopping views when it was created with adadapter
### Added
 - Playrate report

## [6.2.2] - 2018-05-16
### Fix
 - Stop for fatal errors (case errorcode 4)
### Library
 - Packaged with `lib 6.2.6`

## [6.2.1] - 2018-04-16
### Fix
 - Stop for no postrolls ima case with iphone devices
### Library
 - Packaged with `lib 6.2.2`

## [6.2.0] - 2018-04-06
### Fix
 - Included more fatal error cases
### Library
 - Packaged with `lib 6.2.0`

## [6.1.9] - 2018-03-27
### Fix
- Fixed player version 5 and 6 detection to use registerplugin and plugin methods
### Library
- Packaged with `lib 6.1.14`

## [6.1.8] - 2018-03-14
### Fix
- Fixed multiple seek issue
- Packaged with `lib 6.1.13`

## [6.1.8-beta] - 2018-03-05
### Fix
- Fixed onceux issues for brightcove

## [6.1.7] - 2018-02-09
### Fix
- Fixed errorcode = 2 detection, it will close the view for all cases

## [6.1.6] - 2018-01-25
### Fix
- Fixed player access error when setting new adapter (references for unregisterListeners)

## [6.1.5] - 2018-01-11
### Fix
- Controlled an error with hls content without levels or invalid currentLevel for rendition and bitrate

## [6.1.4] - 2018-01-04
### Added
- Packaged with `lib 6.1.8`
- Reverted 6.2.0 beta changes
- Fatal error detection for HLS content.

## [6.2.0-beta] - 2017-12-05
### Added
- Packaged with `lib 6.2.0-beta`
- Added samples for different cases of background detection settings

## [6.1.2] - 2017-10-26
### Added
- Packaged with `lib 6.1.2`

## [6.1.1] - 2017-10-25
### Added
- Packaged with `lib 6.1.1`

## [6.1.0] - 2017-10-23
### Added
- Packaged with `lib 6.1.0`
### Fix
- Fix rendition calculation when using shaka

## [6.0.5] - 2017-10-16
### Added
- Packaged with `lib 6.0.10`
### Fix
- Fix more issues with iphone and ima

## [6.0.4] - 2017-10-10
### Added
- Packaged with `lib 6.0.9`

## [6.0.3] - 2017-10-05
### Added
- Packaged with `lib 6.0.8`
### Fix
- Fix issues with iphone and ima

## [6.0.2] - 2017-10-05
### Added
- Add adapter for freewheel ads

## [6.0.1] - 2017-07-11
### Fix
- Fix problem with manifest and pipelines

## [6.0.0] - 2017-07-10
### Added
- First release

### Smart Plugin
- Packaged with `lib 6.0.4`
