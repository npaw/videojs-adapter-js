var youbora = require('youboralib')
var manifest = require('../../manifest.json')

var OnceUXAdsAdapter = youbora.Adapter.extend({
  getVersion: function () {
    return manifest.version + '-videojs-onceux-ads'
  },

  getResource: function () {
    return this.player.ads.contentSrc
  },

  getPlayhead: function () {
    return this.player.onceux.currentTime()
  },

  getDuration: function () {
    return this.player.onceux.duration()
  },

  getPosition: function () {
    var ret = youbora.Constants.AdPosition.Preroll
    if (this.plugin.getAdapter().flags.isJoined) {
      if (this.plugin.getAdapter().getPlayhead() <= this.plugin.getAdapter().getDuration()) {
        ret = youbora.Constants.AdPosition.Midroll
      } else {
        ret = youbora.Constants.AdPosition.Postroll
      }
    }
    return ret
  },

  registerListeners: function () {
    // Console all events if logLevel=DEBUG
    youbora.Util.logAllEvents(this.player, [
      null,
      'onceux-ads-complete',
      'onceux-adroll-start',
      'onceux-adroll-complete',
      'onceux-linearad-start',
      'onceux-linearad-impression',
      'onceux-linearad-complete',
      'onceux-linearad-skipped',
      'onceux-linearad-pause',
      'onceux-linearad-resume',
      'onceux-companionad-creativeView',
      'adserror',
      'ads-click'
    ])

    // Enable playhead monitor
    this.monitorPlayhead(true, false)

    // Register listeners
    this.references = {
      'onceux-linearad-start': this.startedListener.bind(this),
      'onceux-linearad-pause': this.pausedListener.bind(this),
      'onceux-linearad-resume': this.resumedListener.bind(this),
      'onceux-linearad-complete': this.completeListener.bind(this),
      'onceux-linearad-skipped': this.skippedListener.bind(this),
      adserror: this.errorListener.bind(this),
      'ads-click': this.clickListener.bind(this)
    }
    if (this.player){
      for (var key in this.references) {
        this.player.on(key, this.references[key])
      }
    }
  },

  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()

    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.off(key, this.references[key])
      }
      delete this.references
    }
  },

  startedListener: function (e) {
    this.fireStart()
    this.fireJoin()
  },

  pausedListener: function (e) {
    this.firePause()
  },

  resumedListener: function (e) {
    this.fireResume()
  },

  completeListener: function (e) {
    this.fireStop({ adPlayhead: this.getDuration() })
  },

  skippedListener: function (e) {
    this.fireSkip()
  },

  errorListener: function (e) {
    this.fireError()
  },

  clickListener: function (e) {
    this.fireClick()
  }
},
// Static Members
{
  isUsed: function (plugin) {
    return plugin.player.onceux
  }
}
)

module.exports = OnceUXAdsAdapter
