var freewheel = require('youbora-adapter-freewheel')

var FwAdsAdapter = freewheel.extend({
  register: function () {},
  unregister: function() {}
}, {
  isUsed: function (plugin) {
    return plugin.player && plugin.player.freewheelAds
  }
})

module.exports = FwAdsAdapter
