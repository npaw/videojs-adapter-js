/* global google:true */
var youbora = require('youboralib')
var manifest = require('../../manifest.json')

var ImaAdsAdapter = youbora.Adapter.extend({
  getVersion: function () {
    return manifest.version + '-videojs-ima-ads'
  },

  getDuration: function () {
    if (this.ima.getAdsManager() && this.ima.getAdsManager().getCurrentAd() !== null) {
      this.duration = this.ima.getAdsManager().getCurrentAd().getDuration()
    }
    return this.duration
  },

  // NOT AVAILABLE AT adStart
  getResource: function () {
    var ret = null
    var manager = this.ima.getAdsManager()
    if (manager) {
      ret = manager.getCurrentAd().getMediaUrl()
    }
    return ret
  },

  getTitle: function () {
    var ret = null
    var manager = this.ima.getAdsManager()
    if (manager) {
      ret = manager.getCurrentAd().getTitle()
    }
    return ret
  },

  getPlayhead: function () {
    var ret = null
    var manager = this.ima.getAdsManager()
    if (manager) {
      ret = this.getDuration() - manager.getRemainingTime()
    }
    return ret
  },

  getPlayerVersion: function () {
    return 'IMA' + google.ima.VERSION + '; contrib-ads ' + this.player.ads.VERSION
  },

  getPosition: function () {
    var ret = youbora.Constants.AdPosition.Preroll
    if (this.plugin._adapter.flags.isJoined) {
      if (!this.plugin.getIsLive() && this.plugin._adapter.getDuration() - 1 < this.plugin._adapter.getPlayhead()) {
        ret = youbora.Constants.AdPosition.Postroll
      } else if (this.plugin._adapter.getPlayhead() > 1) {
        ret = youbora.Constants.AdPosition.Midroll
      }
    }
    return ret
  },

  getGivenBreaks: function () {
    var ret = null
    if (this.cuepoints) {
      ret = this.cuepoints.length
    }
    return ret
  },

  getBreaksTime: function () {
    return this.cuepoints
  },

  getGivenAds: function () {
    return this.totalAds
  },

  getAudioEnabled: function () {
    return !this.ima.adMuted
  },

  getIsSkippable: function () {
    return this.isSkippable
  },

  getIsFullscreen: function () {
    var ret = false
    var container = this.ima.adDisplayContainer
    for (var index in container) {
      if (container[index] && container[index].width && container[index].height) {
        if (window.innerHeight && window.innerWidth && container[index].width && container[index].height) {
          ret = (window.innerHeight <= container[index].height + 30 && window.innerWidth <= container[index].width + 30)
        }
      }
    }
    return ret
  },

  getIsVisible: function () {
    return youbora.Util.calculateAdViewability(this.ima.adContainerDiv)
  },

  registerListeners: function () {
    // Enable playhead monitor
    this.monitorPlayhead(true, false)

    // Shortcut events
    var event = google.ima.AdEvent.Type

    // Register listeners
    this.references = {}
    this.references[event.AD_METADATA] = this.metadataListener.bind(this)
    this.references[event.LOADED] = this.loadedListener.bind(this)
    this.references[event.CONTENT_PAUSE_REQUESTED] =
      this.contentPauseRequestedListener.bind(this)
    this.references[event.STARTED] = this.startedListener.bind(this)
    this.references[event.PAUSED] = this.pausedListener.bind(this)
    this.references[event.RESUMED] = this.resumedListener.bind(this)
    this.references[event.COMPLETE] = this.completeListener.bind(this)
    this.references[event.SKIPPED] = this.skippedListener.bind(this)
    this.references[event.CLICK] = this.clickListener.bind(this)
    this.references[google.ima.AdErrorEvent.Type.AD_ERROR] =
      this.errorListener.bind(this)
    this.references[event.ALL_ADS_COMPLETED] = this.adsEndedListener.bind(this)
    this.references[event.FIRST_QUARTILE] = this.firstQuartileListener.bind(this)
    this.references[event.MIDPOINT] = this.secondQuartileListener.bind(this)
    this.references[event.THIRD_QUARTILE] = this.thirdQuartileListener.bind(this)

    this.playerReferences = {
      adend: this.adEndedListener.bind(this),
      adstart: this.startedListener.bind(this),
      adskip: this.skippedListener.bind(this),
      adserror: this.errorListener.bind(this),
      'ads-click': this.clickListener.bind(this)
    }

    if (this.player) {
      this.registerImaEvents()
      for (var key in this.playerReferences) {
        this.player.on(key, this.playerReferences[key])
      }
    }
  },

  registerImaEvents: function () {
    if (this.player.ima.addEventListener) {
      this.ima = this.player.ima
    } else if (this.player.ima.sdkImpl && this.player.ima.sdkImpl.addEventListener) {
      this.ima = this.player.ima.sdkImpl
    }
    if (this.ima) {
      for (var key in this.references) {
        this.ima.addEventListener(key, this.references[key])
      }
    }
  },

  metadataListener: function (e) {
    this.cuepoints = e.getAdCuePoints().getCuePoints().slice()
    if (this.plugin._adapter) {
      this.cuepoints.forEach(function (value, index) {
        if (this.cuepoints[index] === -1 || this.cuepoints[index] == null || this.cuepoints[index] === undefined) {
          this.cuepoints[index] = this.plugin._adapter.getDuration()
        }
      }.bind(this))
    }
  },

  loadedListener: function (e) {
    this.isSkippable = e.getAdData().skippable
    this.totalAds = e.getAdData().adPodInfo.totalAds
    this.fireInit()
  },

  contentPauseRequestedListener: function (e) {
    this.fireInit()
  },

  startedListener: function (e) {
    this.fireStart()
    this.fireJoin()
    this.plugin.getAdapter().firePause()
  },

  pausedListener: function (e) {
    this.firePause()
  },

  resumedListener: function (e) {
    this.fireResume()
  },

  completeListener: function (e) {
    this.fireStop()
  },

  skippedListener: function (e) {
    this.fireSkip()
  },

  adEndedListener: function (e) {
    this.fireStop()
  },

  errorListener: function (e) {
    var error = null
    var code = null
    if (typeof e.getError === 'function') {
      error = e.getError()
      code = error.getCode()
    } else if (e.data && e.data.AdError) {
      error = e.data.AdError
      code = error.getErrorCode()
    }
    var msg = error ? error.getMessage() : null
    this.fireError(code, msg)
  },

  clickListener: function (e) {
    var url = null
    try {
      if (e && typeof e.getAd === 'function') {
        var ad = e.getAd()
        for (prop in ad) {
          if (typeof ad[prop] === 'object' && ad[prop].clickThroughUrl) {
            url = ad[prop].clickThroughUrl.substring(ad[prop].clickThroughUrl.indexOf('adurl=')).replace('adurl=', '')
          }
        }
      }
    } catch (err) {
      youbora.Log.notice('Couldnt get adclick url')
    }
    this.fireClick(url)
  },

  adsEndedListener: function (e) {
    this.plugin.getAdapter().adsEnded = true
    this.fireStop()
    this.plugin.getAdapter().fireResume()
    if (this.plugin.requestBuilder.lastSent.adPosition === youbora.Constants.AdPosition.Postroll) {
      this.plugin.fireStop()
    }
  },

  firstQuartileListener: function (e) {
    this.fireQuartile(1)
  },

  secondQuartileListener: function (e) {
    this.fireQuartile(2)
  },

  thirdQuartileListener: function (e) {
    this.fireQuartile(3)
  },

  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()

    // unregister listeners
    if (this.player && this.ima && this.references && this.playerReferences) {
      for (var key in this.references) {
        // this.ima.removeEventListener(key, this.references[key]) //ima has no remove listeners...
      }
      for (var key in this.playerReferences) {
        this.player.off(key, this.playerReferences[key])
      }
    }
    delete this.references
    delete this.playerReferences
  },

  adPlayerErrorListener: function (e) {
    var error = e.data.AdError
    // https://developers.google.com/interactive-media-ads/docs/sdks/html5/v3/apis#ima.AdError
    var noResponseErrors = [
      1012, // There was a problem requesting ads from the server.
      1005, // There was a problem requesting ads from the server
      301, // The VAST URI provided, or a VAST URI provided in a subsequent wrapper element,
      // was either unavailable or reached a timeout, as defined by the video player. The timeout is 5 seconds for initial VAST requests and each subsequent wrapper.
      402, // Failed to load media assets from a VAST response.
      302 // The maximum number of VAST wrapper redirects has been reached.
    ]
    var emptyResponse = [
      1007, // No assets were found in the VAST ad response
      1009, // Empty VAST response
      303 // No Ads VAST response after one or more wrappers
    ]
    var wrongResponse = [
      403, // Assets were found in the VAST ad response for linear ad, but none of them matched the video player's capabilities
      100, // The ad response was not recognized as a valid VAST ad
      503, // Assets were found in the VAST ad response for nonlinear ad, but none of them matched the video player's capabilities
      101, // VAST schema validation error.
      102 // The ad response contained an unsupported VAST version
    ]
    var code = error.getErrorCode()
    var msg = error.getMessage()
    if (noResponseErrors.indexOf(code) >= 0) {
      this.fireManifest(youbora.Constants.ManifestError.NO_RESPONSE, msg)
    } else if (emptyResponse.indexOf(code) >= 0) {
      this.fireManifest(youbora.Constants.ManifestError.EMPTY, msg)
    } else if (wrongResponse.indexOf(code) >= 0) {
      this.fireManifest(youbora.Constants.ManifestError.WRONG, msg)
    } else {
      this.fireError(code, msg)
    }
  }
},
// Static Members
{
  isUsed: function (plugin) {
    return typeof google !== 'undefined' && google.ima && plugin.player.ima
  }
}
)

module.exports = ImaAdsAdapter
