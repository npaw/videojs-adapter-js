/* global tv */
var youbora = require('youboralib')

var FreewheelAdsAdapter = youbora.Adapter.extend({
  getVersion: function () {
    var ret = 'unknown'
    if (this.player.FreeWheelPlugin.getVersion) {
      ret = 'bcove-fw ' + this.player.FreeWheelPlugin.getVersion()
    } else if (this.player.FreeWheelPlugin.VERSION) {
      ret = 'bcove-ssai ' + this.player.FreeWheelPlugin.VERSION
    }
    return ret
  },

  getDuration: function () {
    var ret = null
    if (this.player.ads && this.player.ads.ad && this.player.ads.ad.duration) {
      ret = this.player.ads.ad.duration
    } else {
      var player = this.plugin.player
      if (this.player.FreeWheelPlugin) {
        var tech = this.player.FreeWheelPlugin.tech
        if (this.player.FreeWheelPlugin[tech].adPlayer) {
          player = this.player.FreeWheelPlugin[tech].adPlayer
        }
      }
      if (player && player.duration) {
        ret = player.duration()
      }
    }
    return ret
  },

  getPosition: function () {
    var ret = youbora.Constants.AdPosition.Midroll
    if (this.player.ads && this.player.ads.ad) {
      switch (this.player.ads.ad.type) {
        case 'PREROLL':
          ret = youbora.Constants.AdPosition.Preroll
          break
        case 'MIDROLL':
          ret = youbora.Constants.AdPosition.Midroll
          break
        case 'POSTROLL':
          ret = youbora.Constants.AdPosition.Postroll
          break
      }
    } else if (!this.plugin._adapter.flags.isJoined) {
      ret = youbora.Constants.AdPosition.Preroll
    } else if (this.plugin._adapter.getPlayhead() >= this.plugin._adapter.getDuration()) {
      ret = youbora.Constants.AdPosition.Postroll
    }
    return ret
  },

  getResource: function () {
    var ret = null
    if (this.slot) {
      var instances = this.slot.getAdInstances()
      if (instances) {
        var adnumber = (this.plugin.requestBuilder.lastSent.adNumber || 1) - 1
        if (instances[adnumber] && instances[adnumber].getActiveCreativeRendition) {
          var creativeRendition = instances[adnumber].getActiveCreativeRendition()
          if (creativeRendition && creativeRendition.getPrimaryCreativeRenditionAsset) {
            var asset = creativeRendition.getPrimaryCreativeRenditionAsset()
            if (asset && asset.getUrl && asset.getUrl()) {
              ret = asset.getUrl()
            }
          }
          if (creativeRendition && creativeRendition.getWrapperUrl && !ret) {
            ret = creativeRendition.getWrapperUrl()
          }
        }
      }
    }
    return ret || 'unknown'
  },

  getTitle: function () {
    var ret = null
    if (this.player.FreeWheelPlugin.adsManager &&
      this.player.FreeWheelPlugin.adsManager.getCurrentAd() &&
      this.player.FreeWheelPlugin.adsManager.getCurrentAd().getTitle()) {
      ret = this.player.FreeWheelPlugin.adsManager.getCurrentAd().getTitle()
    } else if (this.titles) {
      ret = this.titles[(this.plugin.requestBuilder.lastSent.adNumber || 1) - 1]._adId
    }
    return ret
  },

  getPlayhead: function () {
    var player = this.plugin.getAdapter().player
    if (this.player.FreeWheelPlugin.adPlayer) {
      player = this.player.FreeWheelPlugin.adPlayer
    }
    return player.currentTime()
  },

  getGivenBreaks: function () {
    var tech = this.player.FreeWheelPlugin.tech
    var slots = this.player.FreeWheelPlugin[tech]._adContext._adResponse.getTemporalSlots()
    var breaks = 0
    for (var slotindex in slots) {
      var slot = slots[slotindex]
      if (slot.getAdCount() > 0) {
        var ads = slot.getAdInstances()
        for (var adindex in ads) {
          if (ads[adindex]._creativeRenditions[0]._baseUnit === 'video') {
            breaks++
            break
          }
        }
      }
    }
    return breaks
  },

  getBreaksTime: function () {
    var ret = null
    var slots = this.player.FreeWheelPlugin[this.player.FreeWheelPlugin.tech]._adContext._adResponse.getTemporalSlots()
    var times = []
    for (var slotindex in slots) {
      var slot = slots[slotindex]
      if (slot.getAdCount() > 0) {
        var ads = slot.getAdInstances()
        for (var adindex in ads) {
          if (ads[adindex]._creativeRenditions[0]._baseUnit === 'video') {
            if (this.plugin.getDuration() && slot.getTimePosition() > this.plugin.getDuration()) {
              times.push(this.plugin.getDuration())
            } else {
              times.push(slot.getTimePosition())
            }
            break
          }
        }
      }
    }
    ret = times.sort()
    return ret
  },

  getGivenAds: function () {
    var tech = this.player.FreeWheelPlugin.tech
    return this.player.FreeWheelPlugin[tech]._currentSlot.getAdCount()
  },

  getAudioEnabled: function () {
    var ret = null
    if (this.player.muted()) {
      ret = false
    } else {
      ret = !!this.player.volume()
    }
    return ret
  },

  getIsSkippable: function () {
    return false
  },

  getIsFullscreen: function () {
    var ret = null
    if (this.plugin.deviceDetector.isIphone()) {
      ret = !this.plugin.deviceDetector.isInBackground
    } else {
      var container = null
      var tech = this.player.FreeWheelPlugin.tech
      container = this.player.FreeWheelPlugin[tech]._video
      ret = container ? (window.innerHeight <= container.clientHeight + 30 && window.innerWidth <= container.clientWidth + 30) : false
    }
    return ret
  },

  getIsVisible: function () {
    var ret = null
    if (this.plugin.deviceDetector.isIphone()) {
      ret = !this.plugin.deviceDetector.isInBackground
    } else {
      if (!this.contentPlayer) {
        var tech = this.player.FreeWheelPlugin.tech
        for (var key in this.player.FreeWheelPlugin[tech]) {
          var element = this.player.FreeWheelPlugin[tech][key]
          if (element.videoHeight && element.clientHeight) {
            this.contentPlayer = element
            break
          }
        }
      }
      ret = youbora.Util.calculateAdViewability(this.contentPlayer)
    }
    return ret
  },

  getCreativeId: function () {
    var ret = null
    if (this.titles) {
      ret = this.titles[(this.plugin.requestBuilder.lastSent.adNumber || 1) - 1]._creativeId
    }
    return ret
  },

  registerListeners: function () {
    // Enable playhead monitor
    this.monitorPlayhead(true, false)

    this.references = {
      'ads-ad-started': this.startJoinListener.bind(this),
      'ads-pause': this.pausedListener.bind(this),
      'ads-play': this.resumedListener.bind(this),
      'ads-ad-ended': this.adEndedListener.bind(this),
      'ads-pod-ended': this.podEndListener.bind(this),
      'ads-first-quartile': this.firstQuartileListener.bind(this),
      'ads-midpoint': this.secondQuartileListener.bind(this),
      'ads-third-quartile': this.thirdQuartileListener.bind(this)
    }

    if (typeof tv !== 'undefined') {
      var adcontext = this.player.FreeWheelPlugin[this.player.FreeWheelPlugin.tech]._adContext
      this.events = tv.freewheel.SDK

      this.fwreferences = {}
      this.fwreferences[this.events.EVENT_AD] = this.logListener.bind(this)
      this.fwreferences[this.events.EVENT_SLOT_STARTED] = this.slotListener.bind(this)
      this.fwreferences[this.events.EVENT_ERROR] = this.errorFwListener.bind(this)
      this.manifestNoResponse = [
        this.events.ERROR_SECURITY,
        this.events.ERROR_TIMEOUT
      ]
      for (var key in this.manifestNoResponse) {
        this.fwreferences[key] = this.noResponseManifestListener.bind(this)
      }
      this.manifestEmpty = [
        this.events.ERROR_NO_AD_AVAILABLE,
        this.events.ERROR_VAST_NO_AD
      ]
      for (var key2 in this.manifestEmpty) {
        this.fwreferences[key2] = this.manifestEmptyListener.bind(this)
      }
      this.manifestWrong = [
        this.events.ERROR_VAST_VERSION_NOT_SUPPORTED,
        this.events.ERROR_VAST_WRAPPER_LIMIT_REACH,
        this.events.ERROR_VAST_XML_PARSING,
        this.events.ERROR_PARSE
      ]
      for (var key3 in this.manifestWrong) {
        this.fwreferences[key3] = this.manifestWrongListener.bind(this)
      }

      if (this.player) {
        for (var key in this.fwreferences) {
          adcontext.addEventListener(key, this.fwreferences[key])
        }
      }
    }

    if (this.player) {
      for (var key in this.references) {
        this.player.on(key, this.references[key])
      }
    }
  },

  logListener: function (e) {
    if (e.errorCode || e.errorInfo || e.errorModule) {
      this.errorFwListener(e)
      return
    }
    switch (e.subType) {
      case this.events.EVENT_AD_SKIPPED:
        this.skippedListener(e)
        break
      case this.events.EVENT_SLOT_STARTED:
        this.slotListener(e)
        break
      case this.events.EVENT_AD_CLICK:
        this.clickFwListener(e)
        break
    }
  },

  skippedListener: function (e) {
    this.fireSkip()
    if (this.plugin._adapter) this.plugin._adapter.fireResume()
  },

  noResponseManifestListener: function (e) {
    this.fireManifest(youbora.Constants.ManifestError.NO_RESPONSE, 'No response')
  },

  manifestEmptyListener: function (e) {
    this.fireManifest(youbora.Constants.ManifestError.EMPTY, 'Empty manifest')
  },

  manifestWrongListener: function (e) {
    this.fireManifest(youbora.Constants.ManifestError.WRONG, 'Wrong manifest format')
  },

  slotListener: function (e) {
    this.slot = e.slot
    if (e && e.slot && e.slot.getAdInstances) {
      this.titles = e.slot.getAdInstances()
    }
  },

  startJoinListener: function (e) {
    if (this.plugin._adapter) {
      this.plugin._adapter.firePause()
    }
    this.fireStart()
    this.fireJoin()
  },

  pausedListener: function (e) {
    this.firePause()
  },

  resumedListener: function (e) {
    this.fireResume()
  },

  adEndedListener: function (e) {
    var playheadVal = this.plugin.requestBuilder.lastSent.adPosition === youbora.Constants.AdPosition.Preroll ? 0 : this.plugin._adapter.lastPlayhead
    this.fireStop({ playhead: playheadVal, adPlayhead: this.getDuration() })
  },

  errorFwListener: function (e) {
    this.fireError(e.errorCode || e.subType, e.errorInfo)
    this.fireStop()
  },

  clickFwListener: function (e) {
    var url = e.adInstance.getEventCallbackUrls(this.events.EVENT_AD_CLICK, this.events.EVENT_TYPE_CLICK)[0]
    this.fireClick(url)
  },

  firstQuartileListener: function (e) {
    this.fireQuartile(1)
  },

  secondQuartileListener: function (e) {
    this.fireQuartile(2)
  },

  thirdQuartileListener: function (e) {
    this.fireQuartile(3)
  },

  podEndListener: function (e) {
    this.fireBreakStop()
  },

  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()

    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.off(key, this.references[key])
      }
      delete this.references
    }
  }
},
// Static Members
{
  isUsed: function (plugin) {
    return plugin.player.ads && plugin.player.FreeWheelPlugin
  }
})

module.exports = FreewheelAdsAdapter
