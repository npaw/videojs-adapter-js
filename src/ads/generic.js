var youbora = require('youboralib')
var manifest = require('../../manifest.json')

var GenericAdsAdapter = youbora.Adapter.extend({
  isGeneric: true,

  getVersion: function () {
    return manifest.version + '-videojs-ads'
  },

  registerListeners: function () {
    // Register listeners
    this.references = {
      adstart: this.adStartListener.bind(this),
      adend: this.adEndListener.bind(this),
      adskip: this.adSkipListener.bind(this),
      adserror: this.errorListener.bind(this),
      'ads-click': this.clickListener.bind(this)
    }

    if (this.player) {
      for (var key in this.references) {
        this.player.on(key, this.references[key])
      }
    }
  },

  unregisterListeners: function () {
    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.off(key, this.references[key])
      }
      delete this.references
    }
  },

  adStartListener: function (e) {
    this.fireStart()
    this.fireJoin()
  },

  adEndListener: function (e) {
    this.fireStop()
  },

  adSkipListener: function (e) {
    this.fireSkip()
  },

  errorListener: function (e) {
    this.fireError()
  },

  clickListener: function (e) {
    this.fireClick()
  }
})

module.exports = GenericAdsAdapter
