/* global google */
var youbora = require('youboralib')

var FreewheelAdsAdapter = youbora.Adapter.extend({
  getVersion: function () {
    var ret = null
    if (typeof google !== 'undefined' && google.ima) {
      ret = 'bcove-ima ' + google.ima.VERSION
    }
    return ret
  },

  getPlayhead: function () {
    var player = this.plugin.getAdapter().player
    if (this.player.ima3) {
      if (this.player.ima3 && this.player.ima3.adPlayer) {
        player = this.player.ima3.adPlayer
      }
    }
    return player.currentTime()
  },

  getDuration: function () {
    var ret = null
    if (this.player.ads && this.player.ads.ad && this.player.ads.ad.duration) {
      ret = this.player.ads.ad.duration
    } else {
      var player = this.plugin.player
      if (this.player.ima3) {
        player = this.player.ima3.adPlayer
      }
      ret = (player && player.duration) ? player.duration() : null
    }
    return ret
  },

  getPosition: function () {
    var ret = youbora.Constants.AdPosition.Midroll
    if (this.player.ads && this.player.ads.ad) {
      switch (this.player.ads.ad.type) {
        case 'PREROLL':
          ret = youbora.Constants.AdPosition.Preroll
          break
        case 'MIDROLL':
          ret = youbora.Constants.AdPosition.Midroll
          break
        case 'POSTROLL':
          ret = youbora.Constants.AdPosition.Postroll
          break
      }
    } else if (this.plugin._adapter.getPlayhead() >= this.plugin._adapter.getDuration()) {
      ret = youbora.Constants.AdPosition.Postroll
    }
    if (!this.plugin._adapter.flags.isJoined) {
      ret = youbora.Constants.AdPosition.Preroll
    }
    return ret
  },

  getResource: function () {
    var ret = 'unknown'
    if (this.player.ima3) {
      ret = this.player.ima3.currentAd.getMediaUrl()
    }
    return ret
  },

  getTitle: function () {
    var ret = null
    if (this.player.ima3 && this.player.ima3.currentAd) {
      ret = this.player.ima3.currentAd.getTitle()
    }
    return ret
  },

  getPlayerVersion: function () {
    var ret = null
    if (this.player.ima3) {
      ret = this.player.ima3.VERSION
    }
    return ret
  },

  getGivenBreaks: function () {
    var ret = null
    if (this.player.ima3) {
      ret = this.player.ima3.adsManager.getCuePoints().length
    }
    return ret
  },

  getBreaksTime: function () {
    var ret = null
    if (this.player.ima3) {
      var cuelist = this.player.ima3.adsManager.getCuePoints()
      for (var index in cuelist) {
        if ((!cuelist[index] && cuelist[index] !== 0) || cuelist[index] === -1) {
          cuelist[index] = this.plugin._adapter.getDuration()
        }
      }
      ret = cuelist
    }
    return ret
  },

  getGivenAds: function () {
    var ret = null
    if (this.player.ima3 && this.player.ima3.currentAd && this.player.ima3.currentAd.getAdPodInfo()) {
      ret = this.player.ima3.currentAd.getAdPodInfo().getTotalAds()
    }
    return ret
  },

  getAudioEnabled: function () {
    var ret = null
    if (this.player.muted()) {
      ret = false
    } else {
      ret = !!this.player.volume()
    }
    return ret
  },

  getIsSkippable: function () {
    var ret = false
    if (this.player.ima3) {
      ret = this.player.ima3.currentAd && this.player.ima3.currentAd.isSkippable()
    }
    return ret
  },

  getIsFullscreen: function () {
    var ret = null
    if (this.plugin.deviceDetector.isIphone()) {
      ret = !this.plugin.deviceDetector.isInBackground
    } else {
      var container = null
      if (this.player.ima3) {
        container = this.player.ima3.el
      }
      ret = container ? (window.innerHeight <= container.clientHeight + 30 && window.innerWidth <= container.clientWidth + 30) : false
    }
    return ret
  },

  getIsVisible: function () {
    var ret = null
    if (this.plugin.deviceDetector.isIphone()) {
      ret = !this.plugin.deviceDetector.isInBackground
    } else if (this.player.ima3) {
      ret = youbora.Util.calculateAdViewability(this.player.ima3.el)
    }
    return ret
  },

  getBitrate: function () {
    var bitrate = -1
    if (this.player.ima3 && this.player.ima3.currentAd && this.player.ima3.currentAd.getVastMediaBitrate()) {
      bitrate = this.player.ima3.currentAd.getVastMediaBitrate()
    }
    return bitrate
  },

  getCreativeId: function () {
    var ret = null
    if (this.player.ima3 && this.player.ima3.currentAd) {
      ret = this.player.ima3.currentAd.getCreativeAdId() || this.player.ima3.currentAd.getCreativeId()
    } else if (this.titles) {
      ret = this.titles[(this.plugin.requestBuilder.lastSent.adNumber || 1) - 1]._creativeId
    }
    return ret
  },

  registerListeners: function () {
    // Enable playhead monitor
    this.monitorPlayhead(true, false)
    this.references = {
      'ima3-started': this.startJoinListener.bind(this),
      'ima3-paused': this.pausedListener.bind(this),
      'ima3-resumed': this.resumedListener.bind(this),
      'ima3-complete': this.adEndedListener.bind(this),
      'ima3-skipped': this.skippedListener.bind(this),
      'ima3-ad-error': this.errorListener.bind(this),
      'ads-pod-ended': this.podEndListener.bind(this),
      'ads-click': this.clickListener.bind(this),
      'ads-first-quartile': this.firstQuartileListener.bind(this),
      'ads-midpoint': this.secondQuartileListener.bind(this),
      'ads-third-quartile': this.thirdQuartileListener.bind(this)
    }
    if (this.player) {
      for (var key in this.references) {
        this.player.on(key, this.references[key])
      }
    }
  },

  startJoinListener: function (e) {
    if (this.plugin._adapter) {
      this.plugin._adapter.firePause()
    }
    this.fireStart()
    this.fireJoin()
  },

  pausedListener: function (e) {
    this.firePause()
  },

  resumedListener: function (e) {
    this.fireResume()
  },

  skippedListener: function (e) {
    this.fireSkip()
    if (this.plugin._adapter) this.plugin._adapter.fireResume()
  },

  adEndedListener: function (e) {
    var playheadVal = this.plugin.requestBuilder.lastSent.adPosition === youbora.Constants.AdPosition.Preroll ? 0 : this.plugin._adapter.lastPlayhead
    this.fireStop({ playhead: playheadVal, adPlayhead: this.getDuration() })
  },

  errorListener: function (e) {
    if (this.player.ima3) {
      if (e.getError && e.getError()) {
        var error = e.getError()
        this.fireError(error.getCode(), error.getMessage())
      } else {
        this.fireError()
      }
    }
  },

  clickListener: function (e) {
    if (this.player.ima3) {
      var clickurl = null
      if (this.player.ima3.currentAd && this.player.ima3.currentAd.g) {
        clickurl = this.player.ima3.currentAd.g.clickThroughUrl
      }
      this.fireClick(clickurl)
    }
  },

  firstQuartileListener: function (e) {
    this.fireQuartile(1)
  },

  secondQuartileListener: function (e) {
    this.fireQuartile(2)
  },

  thirdQuartileListener: function (e) {
    this.fireQuartile(3)
  },

  podEndListener: function (e) {
    this.fireBreakStop()
  },

  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()

    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.off(key, this.references[key])
      }
      delete this.references
    }
  },

  adPlayerErrorListener: function (e) {
    var error = e.data.AdError
    // https://developers.google.com/interactive-media-ads/docs/sdks/html5/v3/apis#ima.AdError
    var noResponseErrors = [
      1012, // There was a problem requesting ads from the server.
      1005, // There was a problem requesting ads from the server
      301, // The VAST URI provided, or a VAST URI provided in a subsequent wrapper element,
      // was either unavailable or reached a timeout, as defined by the video player. The timeout is 5 seconds for initial VAST requests and each subsequent wrapper.
      402, // Failed to load media assets from a VAST response.
      302 // The maximum number of VAST wrapper redirects has been reached.
    ]
    var emptyResponse = [
      1007, // No assets were found in the VAST ad response
      1009, // Empty VAST response
      303 // No Ads VAST response after one or more wrappers
    ]
    var wrongResponse = [
      403, // Assets were found in the VAST ad response for linear ad, but none of them matched the video player's capabilities
      100, // The ad response was not recognized as a valid VAST ad
      503, // Assets were found in the VAST ad response for nonlinear ad, but none of them matched the video player's capabilities
      101, // VAST schema validation error.
      102 // The ad response contained an unsupported VAST version
    ]
    if (noResponseErrors.indexOf(error.getErrorCode()) >= 0) {
      this.fireManifest(youbora.Constants.ManifestError.NO_RESPONSE, error.getMessage())
    } else if (emptyResponse.indexOf(error.getErrorCode()) >= 0) {
      this.fireManifest(youbora.Constants.ManifestError.EMPTY, error.getMessage())
    } else if (wrongResponse.indexOf(error.getErrorCode()) >= 0) {
      this.fireManifest(youbora.Constants.ManifestError.WRONG, error.getMessage())
    } else {
      this.fireError(error.getErrorCode(), error.getMessage())
    }
  }
},
// Static Members
{
  isUsed: function (plugin) {
    return typeof google !== 'undefined' && plugin.player.ima3 && plugin.player.ads
  }
})

module.exports = FreewheelAdsAdapter
