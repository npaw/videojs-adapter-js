var youboralib = require('youboralib')

module.exports = {
  getHls: function(adapter) {
    return adapter.getTech().vhs || adapter.getTech().hls || adapter.getTech().hls_
  },

  isUsed: function (adapter) {
    return !!this.getHls(adapter)
  },

  getBitrate: function (adapter) {
    var ret = null
    var media = this.getHls(adapter).playlists.media()
    if (adapter.player.qualityLevels) {
      // using videojs-contrib-quality-levels
      // https://github.com/videojs/videojs-contrib-quality-levels
      var qualityLevels = adapter.player.qualityLevels()
      var selected = qualityLevels[qualityLevels.selectedIndex]
      if (selected) {
        ret = selected.bitrate
      }
    } else if (media && media.attributes && media.attributes.BANDWIDTH) {
      ret = media.attributes.BANDWIDTH
    } else if (adapter.player.tech_ && adapter.player.tech_.vhs && adapter.player.tech_.vhs.stats) {
      ret = adapter.player.tech_.vhs.stats.bandwidth
    } else if (adapter.player.tech_ && adapter.player.tech_.hls && adapter.player.tech_.hls.bandwidth) {
      ret = adapter.player.tech_.hls.bandwidth
    } else if (adapter.player.hls && adapter.player.hls.bandwidth) {
      ret = adapter.player.hls.bandwidth
    }
    return ret
  },

  getRendition: function (adapter) {
    var ret = null
    var media = this.getHls(adapter).playlists.media()
    if (adapter.player.qualityLevels) {
      // using videojs-contrib-quality-levels
      // https://github.com/videojs/videojs-contrib-quality-levels
      var qualityLevels = adapter.player.qualityLevels()
      var selected = qualityLevels[qualityLevels.selectedIndex]
      if (selected) {
        ret = youboralib.Util.buildRenditionString(selected.width, selected.height, selected.bitrate)
      }
    } else if (media && media.attributes) {
      var att = media.attributes
      if (att.RESOLUTION) {
        ret = youboralib.Util.buildRenditionString(
          att.RESOLUTION.width,
          att.RESOLUTION.height,
          att.BANDWIDTH
        )
      } else if (att.BANDWIDTH) {
        ret = youboralib.Util.buildRenditionString(att.BANDWIDTH)
      } else if (att.NAME) {
        ret = att.NAME
      }
    }
    return ret
  },

  getTotalBytes: function (adapter) {
    var hls = this.getHls(adapter)
    var ret = hls.bytesReceived
    if (!ret && hls.stats) {
      ret = hls.stats.mediaBytesTransferred - adapter.acumBytes
    }
    return ret
  }

}
