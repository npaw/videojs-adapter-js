var youboralib = require('youboralib')

module.exports = {
  getHls: function(adapter) {
    return this.getTech()
  },

  isUsed: function (adapter) {
    return !!this.getTech(adapter)
  },

  getResource: function (adapter) {
    var ret = null
    if (this.getTech(adapter) && this.getTech(adapter).url) {
      ret = this.getTech(adapter).url
    } else if (this.getHls(adapter) && this.getHls(adapter).source_) {
      ret = this.getHls(adapter).source_.src
    }
    return ret
  },

  getBitrate: function (adapter) {
    var ret = null
    var currentLevel = this.getTech(adapter).currentLevel
    if (typeof currentLevel !== 'undefined' && currentLevel !== -1 && !!this.getTech(adapter).levels) {
      var level = this.getTech(adapter).levels[currentLevel]
      if (level && level.bitrate) {
        ret = level.bitrate
      }
    }
    return ret
  },

  getRendition: function (adapter) {
    var ret = null
    var currentLevel = this.getTech(adapter).currentLevel
    if (typeof currentLevel !== 'undefined' && currentLevel !== -1 && !!this.getTech(adapter).levels) {
      var level = this.getTech(adapter).levels[currentLevel]
      if (level) {
        ret = youboralib.Util.buildRenditionString(level.width, level.height, level.bitrate)
      }
    }
    return ret
  },

  getTech: function(adapter) {
    var tech = adapter.getTech()
    return tech.vhs || tech.hls || tech.hls_
  }
}
