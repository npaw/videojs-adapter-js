var youboralib = require('youboralib')

module.exports = {
  isUsed: function (adapter) {
    return adapter.player.dash && adapter.player.dash.shakaPlayer
  },

  getResource: function (adapter) {
    var ret = ''
    var sp = adapter.player.dash.shakaPlayer
    if (typeof sp.getManifestUri === 'function'){
      ret = sp.getManifestUri()
    } else if (typeof sp.getAssetUri === 'function') {
      ret = sp.getAssetUri()
    }
    return ret
  },

  getBitrate: function (adapter) {
    return adapter.player.dash.shakaPlayer.getStats().streamBandwidth
  },

  getRendition: function (adapter) {
    var shaka = adapter.player.dash.shakaPlayer
    var stats = shaka.getStats()
    var ret = youboralib.Util.buildRenditionString(stats.width, stats.height, stats.streamBandwidth)
    var tracks = shaka.getVariantTracks()
    for (var i in tracks) {
      var track = tracks[i]
      if (track.active && track.type === 'video') {
        ret = youboralib.Util.buildRenditionString(track.width, track.height, track.bandwidth)
      }
    }
    return ret
  }
}
