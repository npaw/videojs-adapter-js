var youboralib = require('youboralib')

module.exports = {
  isUsed: function (adapter) {
    return adapter.player.dash && adapter.player.dash.mediaPlayer && !adapter.player.dash.shakaPlayer
  },

  getResource: function (adapter) {
    return this._getDashPlayer(adapter).getSource()
  },

  getBitrate: function (adapter) {
    var ret = null
    var player = this._getDashPlayer(adapter)
    if (player) {
      var level = player.getQualityFor('video')
      if (level) {
        ret = player.getBitrateInfoListFor('video')[level].bitrate
      }
    }
    return ret
  },

  getRendition: function (adapter) {
    var ret = null
    var player = this._getDashPlayer(adapter)
    if (player) {
      var level = player.getQualityFor('video')
      if (level) {
        var renditionInfo = player.getBitrateInfoListFor('video')[level]
        if (renditionInfo) {
          ret = youboralib.Util.buildRenditionString(renditionInfo.width, renditionInfo.height, renditionInfo.bitrate)
        }
      }
    }
    return ret
  },

  _getDashPlayer: function(adapter) {
    return adapter.player.dash.mediaPlayer
  }
}
