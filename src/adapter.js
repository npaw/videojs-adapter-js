/* global videojs */
var youbora = require('youboralib')
var manifest = require('../manifest.json')

var VideojsAdapter = youbora.Adapter.extend({
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  getTitle: function () {
    var ret = null
    if (this.player.mediainfo) {
      ret = this.player.mediainfo.name
    }
    return ret
  },

  getPlayhead: function () {
    var ret = this.player.currentTime()
    if (this.plugin._adsAdapter && this.plugin._adsAdapter.flags.isStarted) {
      ret = this.lastPlayhead || ret
    }
    return ret
  },

  getIsLive: function () {
    var ret = null
    if (this.player.mediainfo) {
      ret = true
      if (typeof this.player.mediainfo.duration === 'number') {
        ret = this.player.mediainfo.duration < 1
      }
    }
    return ret
  },

  getDuration: function () {
    var ret = this.player.duration()
    if (this.player.mediainfo && typeof this.player.mediainfo.duration !== 'undefined') {
      ret = this.player.mediainfo.duration
    }
    return ret
  },

  getResource: function () {
    var ret = null
    var tech = this.getUsedTech()
    if (tech && tech.getResource) {
      ret = tech.getResource(this)
    }
    if (!ret) {
      ret = this.player.currentSrc()
    }
    return ret
  },

  getBitrate: function () {
    var ret = null
    var tech = this.getUsedTech()
    if (tech && tech.getBitrate) {
      ret = tech.getBitrate(this)
    }
    return ret
  },

  getRendition: function () {
    var ret = null
    var tech = this.getUsedTech()
    if (tech && tech.getRendition) {
      ret = tech.getRendition(this)
    }
    return ret
  },

  getTotalBytes: function () {
    var ret = null
    var tech = this.getUsedTech()
    if (tech && tech.getTotalBytes) {
      ret = tech.getTotalBytes(this)
    }
    return ret
  },

  getPlayerName: function () {
    var name = 'videojs'
    if (VideojsAdapter.ContribHlsTech.isUsed(this)) {
      name += '-hls' // hls-contrib
    } else if (VideojsAdapter.HlsJsTech.isUsed(this)) {
      name += '-hlsjs' // hlsjs
    } else if (VideojsAdapter.ShakaTech.isUsed(this)) {
      name += '-shaka' // shaka
    } else if (VideojsAdapter.DashShakaTech.isUsed(this)) {
      name += '-dash-shaka' // dash shaka
    } else if (VideojsAdapter.DashTech.isUsed(this)) {
      name += '-dash' // dash
    }
    if (VideojsAdapter.ImaAdsAdapter.isUsed(this)) {
      name += '-ima' // ima3
    } else if (VideojsAdapter.OnceUXAdsAdapter.isUsed(this)) {
      this.pluginName += '-oux' // OnceUX
    } else if (this.player.FreeWheelPlugin) { // TO CHANGE
      name += '-fw' // freewheel
    }
    if (this.player.mediainfo) {
      name += '-bcove' // brightcove
    }
    return name
  },

  getPlayerVersion: function () {
    var ret = null
    if (typeof videojs !== 'undefined') {
      ret = videojs.VERSION
    }
    return ret
  },

  getPlayrate: function () {
    return this.player.playbackRate()
  },

  isOnAdBreak: function() {
    try {
      return (this.player && this.player.ads && this.player.ads.inAdBreak && this.player.ads.inAdBreak())
    } catch (e) {}

    return false
  },

  getTech: function () {
    // NOTE: Videojs discourages accessing techs from plugins because they want
    // devs to develop tech-agnostic plugins. I don't think this applies to us,
    // as we need to retrieve info from each different tech.
    // https://github.com/videojs/video.js/issues/2617
    return this.player.tech({ IWillNotUseThisInPlugins: true })
  },

  getUsedTech: function () {
    var ret = null
    if (VideojsAdapter.ContribHlsTech.isUsed(this)) {
      ret = VideojsAdapter.ContribHlsTech
    } else if (VideojsAdapter.HlsJsTech.isUsed(this)) {
      ret = VideojsAdapter.HlsJsTech
    } else if (VideojsAdapter.ShakaTech.isUsed(this)) {
      ret = VideojsAdapter.ShakaTech
    } else if (VideojsAdapter.DashShakaTech.isUsed(this)) {
      ret = VideojsAdapter.DashShakaTech
    } else if (VideojsAdapter.DashTech.isUsed(this)) {
      ret = VideojsAdapter.DashTech
    }
    return ret
  },

  registerListeners: function () {
    // Enable playhead monitor
    this.monitorPlayhead(true, false, 1200)
    this.acumBytes = 0
    this.ignoreList = []
    this.fatalList = []
    this.nonFatalList = []
    // Register listeners
    this.references = {
      loadstart: this.loadstartListener.bind(this),
      adsready: this.loadAdsAdapter.bind(this),
      play: this.playListener.bind(this),
      timeupdate: this.timeupdateListener.bind(this),
      pause: this.pauseListener.bind(this),
      playing: this.playingListener.bind(this),
      // abort: this.conditionalStop.bind(this),
      ended: this.conditionalStop.bind(this),
      dispose: this.disposeListener.bind(this),
      seeking: this.seekingListener.bind(this),
      seeked: this.seekedListener.bind(this),
      error: this.errorListener.bind(this),
      adserror: this.aderrorListener.bind(this),
      adsEnded: this.adsEndedListener.bind(this),
      texttrackchange: this.textListener.bind(this),
      waiting: this.bufferingListener.bind(this)
    }

    if (this.player) {
      for (var key in this.references) {
        this.player.on(key, this.references[key])
      }
    }

    setTimeout(this.loadAdsAdapter.bind(this), 50)
  },

  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()
    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.off(key, this.references[key])
      }
      delete this.references
    }
  },

  textListener: function (e) {
    if (this.player.textTracks) {
      var tracks = this.player.textTracks()
      var len = tracks.length
      for (var i = 0; i < len; ++i) {
        if (tracks[i] && tracks[i].mode === 'showing') {
          this.plugin.options['content.subtitles'] = tracks[i].label || tracks[i].language
        }
      }
    }
  },

  aderrorListener: function (e) {
    this.loadAdsAdapter()
    if (this.plugin.getAdsAdapter() && this.plugin.getAdsAdapter().adPlayerErrorListener) {
      this.plugin.getAdsAdapter().adPlayerErrorListener(e)
    }
  },

  loadstartListener: function (e) {
    this.loadAdsAdapter()
    youbora.Log.notice('Player detected ' + this.getPlayerName())
  },

  playListener: function (e) {
    if (this.isOnAdBreak()) {
      return
    }
    if (!this.flags.isStarted) {
      this.lastSrc = this.getResource()
      this._startEvent('playListenerNotStarted')
    } else {
      if (this.lastSrc && this.lastSrc !== this.getResource()) {
        this.fireStop({}, 'playListenerNewResource')
        this.acumBytesFunc()
        this.lastPlayhead = undefined
        this._startEvent('playListenerNewResource')
        this.lastSrc = this.getResource()
      }
    }
  },

  _startEvent: function (triggerListener) {
    this.loadAdsAdapter()
    if (this.player.options_ && this.plugin && !this.plugin.options['content.id']){
      this.plugin.options['content.id'] = this.player.options_['data-video-id']
    }
    this.fireStart({}, triggerListener)
    if (!this.flags.isJoined) {
      if (!this.joinPolling) {
        this.joinPolling = new youbora.Timer(this._jointimePolling.bind(this), 500)
      }
      this.joinPolling.start()
    }
  },

  _jointimePolling: function (e) {
    if (this.plugin) {
      if (!this.plugin.getAdsAdapter() || !this.plugin.getAdsAdapter().flags.isStarted) {
        if (this.lastPlayhead !== 0 && typeof this.lastPlayhead === 'number' && this.lastPlayhead !== this.getPlayhead()) {
          this.fireJoin({}, 'jointimePolling')
          if (this.joinPolling) {
            this.joinPolling.stop()
          }
        }
        this.lastPlayhead = this.getPlayhead()
      }
    } else if (this.joinPolling) {
      this.joinPolling.stop()
    }
  },

  timeupdateListener: function (e) {
    if (this.getPlayhead() > 0.1) {
      this.fireJoin({}, 'timeupdateListener')
      if (this.joinPolling) this.joinPolling.stop()
      // Send seek end
      if (!this.flags.isPaused && this.lastSeekPlayhead && this.lastSeekPlayhead !== this.getPlayhead()) {
        this.fireSeekEnd({}, 'timeupdateListener')
        this.lastSeekPlayhead = false
      }
    }
    if (!this.flags.isJoined) {
      if (this.lastPlayhead === undefined) {
        this.lastPlayhead = this.getPlayhead()
      }
      if (this.lastPlayhead !== this.getPlayhead()) {
        if (this.player.options_ && this.plugin && !this.plugin.options['content.id']){
          this.plugin.options['content.id'] = this.player.options_['data-video-id']
        }
        this.fireStart({}, 'timeupdateListener')
        this.fireJoin({}, 'timeupdateListener')
        if (this.joinPolling) this.joinPolling.stop()
        this.lastPlayhead = undefined
      }
    }
    if (this.plugin._adsAdapter && !this.plugin._adsAdapter.flags.isStarted) {
      this.lastPlayhead = this.getPlayhead()
    }
  },

  pauseListener: function (e) {
    this.firePause({}, 'pauseListener')
  },

  playingListener: function (e) {
    this._startEvent('playingListener')
    this.fireResume({}, 'playingListener')
    if (this.getPlayhead() < 1) {
      this.fireSeekEnd({}, 'playingListener')
    }
  },

  disposeListener: function(e) {
    if (this.plugin) {
      this.plugin.fireStop({}, 'disposeListener')
      this.plugin.removeAdsAdapter()
      this.plugin.removeAdapter()
    }
  },

  conditionalStop: function (e) {
    if (this.isOnAdBreak()) {
      return
    }
    if (this.plugin) {
      this.adsEnded = false
      this.fireStop({}, 'conditionalStop')
      if (this.plugin && !this.plugin.options['content.id']){
        this.plugin.options['content.id'] = null
      }
      this.acumBytesFunc()
      this.lastPlayhead = undefined
    }
  },

  adsEndedListener: function (e) {
    this.adsEnded = true
    if (this.plugin.requestBuilder.lastSent.adPosition === youbora.Constants.AdPosition.Postroll) {
      this.plugin.fireStop({}, 'adsEndedListener')
      this.acumBytesFunc()
      this.lastPlayhead = undefined
    }
  },

  seekingListener: function (e) {
    this.fireSeekBegin({}, false, 'seekingListener')
  },

  seekedListener: function (e) {
    // We save the playhead after the seek, we will send the seeked in the next timeupdate
    this.lastSeekPlayhead = this.getPlayhead()
  },

  bufferingListener: function (e) {
    this.fireBufferBegin({}, false, 'bufferingListener')
  },

  errorListener: function (e) {
    var ignore = false
    var fatal = false
    var code = null
    var msg = null

    if (this.player.error && this.player.error()) {
      code = Number(this.player.error().code)
      msg = this.player.error().message
      if (code === 2 || code === 4 || code < 0) {
        fatal = true
      }
    }

    if (code !== null) {
      if (this.ignoreList.includes(code)) {
        ignore = true
      }
      if (!fatal && this.fatalList.includes(code)) {
        fatal = true
      } else if (fatal && this.nonFatalList.includes(code)) {
        fatal = false
      }
    }

    if (!ignore) {
      if (fatal) {
        this.fireFatalError(code, msg, undefined, undefined, 'errorListener')
        this.acumBytesFunc()
        this.lastPlayhead = undefined
        this.crashed = true
      } else {
        this.fireError(code, msg, undefined, undefined, 'errorListener')
      }
    }
  },

  loadAdsAdapter: function () {
    if (this.plugin) {
      if (this.plugin.getAdsAdapter() === null || this.plugin.getAdsAdapter().isGeneric) {
        var adapter = null
        // IMA
        if (VideojsAdapter.ImaAdsAdapter.isUsed(this)) {
          adapter = new VideojsAdapter.ImaAdsAdapter(this.player)
          // OnceUX
        } else if (VideojsAdapter.OnceUXAdsAdapter.isUsed(this)) {
          adapter = new VideojsAdapter.OnceUXAdsAdapter(this.player)
          // Ima Bcove
        } else if (VideojsAdapter.BcoveImaAdsAdapter.isUsed(this)) {
          adapter = new VideojsAdapter.BcoveImaAdsAdapter(this.player)
          // Freewheel Bcove
        } else if (VideojsAdapter.BcoveFreewheelAdsAdapter.isUsed(this)) {
          adapter = new VideojsAdapter.BcoveFreewheelAdsAdapter(this.player)
          // Freewheel
        } else if (VideojsAdapter.FreewheelAdsAdapter.isUsed(this)) {
          adapter = new VideojsAdapter.FreewheelAdsAdapter(this.player.freewheelAds.controller)
          // Generic
        } else if (this.plugin.getAdsAdapter() === null) {
          adapter = new VideojsAdapter.GenericAdsAdapter(this.player)
        }
        if (adapter) this.plugin.setAdsAdapter(adapter)
      } else {
        if (this.plugin.getAdsAdapter().registerImaEvents) this.plugin.getAdsAdapter().registerImaEvents()
      }
    }
  },

  acumBytesFunc: function () {
    this.acumBytes += this.getTotalBytes() || 0
  },

  errorModifyer: function (fatal, nonfatal, ignore) {
    this.fatalList = fatal || []
    this.nonFatalList = nonfatal || []
    this.ignoreList = ignore || []
  }
},
// Static members
{
  // Ads Adaptrs
  GenericAdsAdapter: require('./ads/generic'),
  BcoveImaAdsAdapter: require('./ads/bcove-ima'),
  BcoveFreewheelAdsAdapter: require('./ads/bcove-freewheel'),
  FreewheelAdsAdapter: require('./ads/freewheel'),
  ImaAdsAdapter: require('./ads/ima'),
  OnceUXAdsAdapter: require('./ads/onceux'),

  // Techs
  ContribHlsTech: require('./tech/contrib-hls'),
  HlsJsTech: require('./tech/hls-js'),
  ShakaTech: require('./tech/shaka'),
  DashShakaTech: require('./tech/dash-shaka'),
  DashTech: require('./tech/dash')
}
)

youbora.adapters.Videojs = VideojsAdapter

module.exports = youbora.adapters.Videojs
